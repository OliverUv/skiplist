use std::mem;
use std::ptr;

use skiplist::{SkipList, Node};

pub struct Iter<'a, T: 'a> {
    inner: Option<&'a Node<T>>,
}

impl<'a, T> IntoIterator for &'a SkipList<T> {
    type Item = &'a T;
    type IntoIter = Iter<'a, T>;

    fn into_iter(self) -> Iter<'a, T> {
        Iter {
            inner: unsafe { self.lanes.get_unchecked(0).read() },
        }
    }
}

impl<'a, T> Iterator for Iter<'a, T> {
    type Item = &'a T;

    fn next(&mut self) -> Option<&'a T> {
        self.inner.take().map(|node| {
            self.inner = unsafe { node.lanes().get_unchecked(0).read() };
            node.elem()
        })
    }
}

pub struct IntoIter<T> {
    ptr: *mut Node<T>,
}

impl<T> IntoIterator for SkipList<T> {
    type Item = T;
    type IntoIter = IntoIter<T>;

    fn into_iter(self) -> IntoIter<T> {
        unsafe {
            let ptr = mem::transmute(ptr::read(self.lanes.get_unchecked(0)));
            mem::forget(self);
            IntoIter { ptr }
        }
    }
}

impl<T> Iterator for IntoIter<T> {
    type Item = T;
    
    fn next(&mut self) -> Option<T> {
        if self.ptr as usize == 0 {
            None
        } else {
            unsafe {
                let elem = ptr::read((*self.ptr).elem());
                let next: *mut Node<T> = mem::transmute((*self.ptr).lanes().get_unchecked(0).read());
                let old = mem::replace(&mut self.ptr, next);
                (*old).dealloc();
                Some(elem)
            }
        }
    }
}
